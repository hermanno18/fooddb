# food-database

Documentation

## Pour installer  le projet et ses dépendances

```
 npm install
```

### Démarer le serveur

```
npm run serve
```

## Utiliser l'application

Une fois le serveur en marche, l'utilisateur se retrouve sur la page d'acceuil, mais ne peux faire aucune recherche, car la base de donnée est vide pour l'instant. 
L'administrateur doit d'abord charger les données du fichier Excel. Pour cela, suivez ces étapes:

- ### Connexion au paneau d'administration
    C'est tres simple : il siffit de cliqer sur "Connexion dans le menu" et de remplir le formulaire.  
    `Note`: un administrateur par défaut a été  créé en dur, ce sont ces paramettres que vous devez utiliser. 
    ```
         "email" : admin@fooddb.com
         "password" : 123456789 
    ```
- ### chargement des données
    une fois connecté au Dashboard, vous serrez invité à charger le fichier excel. suivez les sous-étapes suivantes dans l'ordre.
     - cliquer sur le input:file  qui vous invite à séléectionner votre fichier exel dans vos documents sur votre ordinateur. (`Ǹote:` faites attention à sélectionner le bon fichier par ce que sinon, l'application ne fonctionnera pas)
     - patienté que le ficher charge, puis un champ sélect apparaitra sur la partie droite de l'écran. cliquez dessus et sélectionnez la premiere proposition (celle qui porte le nom du ficher)
     - patientez encore quelques secondes (5secondes environs) le temps que le script décode le ficher Excel.
     - Une fois terminé, un tableau contenant toutes les données du fichier apparaitra 
     - cliquer enfin sur le bouton "charger en BD" pour permettre à VueJS de stoquer les données sur le navigateur.
     - C'est bon ! Vous pouvez faire les recherches dans le fichier et voir tous les repas!! 
